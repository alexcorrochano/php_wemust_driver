<?php

declare(strict_types=1);

namespace Wemust\Test;

use Wemust\WemustAPI;
use Wemust\Factories\WemustV1 as RequestFactory;
use Wemust\Managers\WemustAPIv1 as RequestManager;
use Wemust\Service\WemustAPIv1 as RequestService;
use PHPUnit\Framework\TestCase;

final class GetUserListTest extends TestCase
{
    private function getDriver(): \Wemust\WemustAPI
    {
        $this->baseUrl      = 'https://dcadmin-wmweb.must.edu.mo:9003/api/';
        $this->serviceCode  = 'WM-MOODLE';
        $this->clientID     = 'wm7353b34c389e4178';
        $this->clientSecret = '528dbcc24e244dba89f269c40cb528da';

        $requestFactory = new RequestFactory(
            $this->baseUrl,
            $this->serviceCode,
            $this->clientID,
            $this->clientSecret
        );
        $requestService = new RequestService();
        $requestManager = new RequestManager($requestService);

        return new WemustAPI($requestFactory, $requestManager);
    }

    public function testGetListNotEmpty(): void
    {
        $wemust   = $this->getDriver();
        $response = $wemust('GetUserList', ['offsetStart' => 1, 'maxPageItems' => 10]);
        $this->assertSame($response::STATUS_OK, $response->getStatus());

        $data = $response->getData();
        $this->assertArrayHasKey('list', $data);
        $this->assertCount(10, $data['list']);

        $this->assertArrayHasKey('total', $data);
        $this->assertGreaterThan(10, $data['total']);
    }

    public function testGetListOutOfContent(): void
    {
        $wemust   = $this->getDriver();
        $response = $wemust('GetUserList', ['offsetStart' => 80000, 'maxPageItems' => 10]);
        $this->assertSame($response::STATUS_OK, $response->getStatus());

        $data = $response->getData();
        $this->assertArrayHasKey('list', $data);
        $this->assertCount(0, $data['list']);

        $this->assertArrayHasKey('total', $data);
        $this->assertGreaterThan(10, $data['total']);
    }

    public function testGetListOffsetZero(): void
    {
        $wemust   = $this->getDriver();
        $response = $wemust('GetUserList', ['offsetStart' => -1, 'maxPageItems' => 10]);
        $this->assertSame($response::STATUS_OK, $response->getStatus());

        $data = $response->getData();
        $this->assertArrayHasKey('list', $data);
        $this->assertCount(0, $data['list']);

        $this->assertArrayHasKey('total', $data);
        $this->assertGreaterThan(10, $data['total']);
    }

    public function testGetListOffsetLessThanZero(): void
    {
        $wemust   = $this->getDriver();
        $response = $wemust('GetUserList', ['offsetStart' => -1, 'maxPageItems' => 10]);
        $this->assertSame($response::STATUS_OK, $response->getStatus());

        $data = $response->getData();
        $this->assertArrayHasKey('list', $data);
        $this->assertCount(0, $data['list']);

        $this->assertArrayHasKey('total', $data);
        $this->assertGreaterThan(10, $data['total']);
    }
}
