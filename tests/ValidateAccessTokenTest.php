<?php

declare(strict_types=1);

namespace Wemust\Test;

use DateTime;
use DomainException;
use Wemust\Domain\Entities\AccessToken;
use PHPUnit\Framework\TestCase;

final class ValidateAccessTokenTest extends TestCase
{
    public function testTokenExpired(): void
    {
        $accessToken = new AccessToken('abcde', '1979-01-17 00:00:00', 7200);
        $this->assertSame(false, $accessToken->isValid());
    }

    public function testGetValidToken(): void
    {
        $now         = new DateTime();
        $accessToken = new AccessToken('abcde', $now->format('Y-m-d H:i:s'), 7200);
        $this->assertSame('abcde', $accessToken->getAccessToken());
    }

    public function testGetTokenExpired(): void
    {
        $accessToken = new AccessToken('abcde', '1979-01-17 00:00:00', 7200);
        try {
            $accessToken = $accessToken->getAccessToken();
            $this->$this->assertEmpty($accessToken);
        } catch (DomainException $exception) {
            $this->assertSame(AccessToken::ERR_MSG_TOKEN_NOT_VALID, $exception->getMessage());
        }
    }
}
