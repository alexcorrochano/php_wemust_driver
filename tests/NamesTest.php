<?php

declare(strict_types=1);

namespace Wemust\Test;

use DateTime;
use DomainException;
use Wemust\Domain\Entities\Name;
use PHPUnit\Framework\TestCase;

final class NamesTest extends TestCase
{
    public function testEnNameWithCommas(): void
    {
        $nameEn = 'ALEXANDER ANGEL CORROCHANO';
        $nameZh = 'ALEXANDER ANGEL CORROCHANO';

        $nameEntity = new Name($nameEn, $nameZh);

        $this->assertSame('Alexander', $nameEntity->getFirstNameEn());
        $this->assertSame('Angel Corrochano', $nameEntity->getLastNameEn());
        $this->assertSame('', $nameEntity->getFirstNameZh());
        $this->assertSame('', $nameEntity->getLastNameZh());
    }

    public function testZhNameWithCommas(): void
    {
        $nameEn = 'NG, KIT MAN (ANNA)';
        $nameZh = '吳潔雯';

        $nameEntity = new Name($nameEn, $nameZh);

        $this->assertSame('Kit Man (Anna)', $nameEntity->getFirstNameEn());
        $this->assertSame('Ng', $nameEntity->getLastNameEn());
        $this->assertSame('潔雯', $nameEntity->getFirstNameZh());
        $this->assertSame('吳', $nameEntity->getLastNameZh());
    }

    public function testZhNameWithCommasAndParenthesis(): void
    {
        $nameEn = 'WANG, TING (PILAR)';
        $nameZh = '王婷';

        $nameEntity = new Name($nameEn, $nameZh);

        $this->assertSame('Ting (Pilar)', $nameEntity->getFirstNameEn());
        $this->assertSame('Wang', $nameEntity->getLastNameEn());
        $this->assertSame('婷', $nameEntity->getFirstNameZh());
        $this->assertSame('王', $nameEntity->getLastNameZh());
    }

    public function testEnNameWithDifferentNames(): void
    {
        $nameEn = 'ANAND, BHASKAR';
        $nameZh = 'ANAND,BHASKAR';

        $nameEntity = new Name($nameEn, $nameZh);

        $this->assertSame('Bhaskar', $nameEntity->getFirstNameEn());
        $this->assertSame('Anand', $nameEntity->getLastNameEn());
        $this->assertSame('', $nameEntity->getFirstNameZh());
        $this->assertSame('', $nameEntity->getLastNameZh());
    }

    public function testEnNameGetSimpleFirstName(): void
    {
        $nameEn = 'ALEXANDER ANGEL CORROCHANO';
        $nameZh = 'ALEXANDER ANGEL CORROCHANO';

        $nameEntity = new Name($nameEn, $nameZh);

	var_dump($nameEntity->getFullName());
	var_dump(Name::extractNameParts($nameEn, $nameZh));

        $this->assertSame(
            'Alexander',
            $nameEntity->getSimpleFirstname(),
            'We extract incorrect simple first name from English name'
        );
    }
}
