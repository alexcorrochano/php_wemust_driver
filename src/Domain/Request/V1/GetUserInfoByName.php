<?php

namespace Wemust\Domain\Request\V1;

use Wemust\Domain\Entities\Name;
use Wemust\Domain\Request\Base;
use Wemust\Domain\Request\Interfaces;

class GetUserInfoByName extends Base implements Interfaces
{
    public function __construct(array $requestParameters, ...$params)
    {
        parent::__construct(...$params);
        $this->sufixUrl            = 'v1/user/getUserInfoByUsername/';
        $this->needAuth            = true;
        $this->mandatoryParameters = [
            'accessToken'     => 'String',
            'serviceCode'     => 'String',
            'username'        => 'String',
        ];

        $this->setParameters($requestParameters);
    }

    public function formatResult(array $result): array
    {
        if (isset($result['nameEn']) && isset($result['name'])) {
            $name                   = new Name($result['nameEn'], $result['name']);
            $result['formatedName'] = $name->getFullName();
        }

        return $result;
    }
}
