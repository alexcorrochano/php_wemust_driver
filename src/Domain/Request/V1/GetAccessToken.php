<?php

namespace Wemust\Domain\Request\V1;

use Wemust\Domain\Request\Base;
use Wemust\Domain\Request\Interfaces;

class GetAccessToken extends Base implements Interfaces
{
    public function __construct(array $requestParameters, ...$params)
    {
        parent::__construct(...$params);
        $this->sufixUrl            = 'v1/common/getAccessToken/';
        $this->needAuth            = false;
        $this->mandatoryParameters = [
            'serviceCode'  => 'String',
            'clientId'     => 'String',
            'clientSecret' => 'String',
        ];

        $this->setParameters($requestParameters);
    }

    public function formatResult(array $result): array
    {
        return $result;
    }
}
