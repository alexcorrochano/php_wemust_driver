<?php

declare(strict_types=1);

namespace Wemust\Domain\Request;

use DomainException;
use Wemust\Domain\Entities\AccessToken;

class Base
{
    protected string $baseUrl;
    protected string $serviceCode;
    protected string $clientId;
    protected string $clientSecret;
    protected AccessToken $accessToken;
    protected bool $needAuth             = true;
    protected array $mandatoryParameters = [];
    protected array $optionalParameters  = [];
    protected array $parameters          = [];
    protected array $headers             = [];

    public function __construct(string $baseUrl, $serviceCode, $clientId, $clientSecret)
    {
        $this->baseUrl      = $baseUrl;
        $this->serviceCode  = $serviceCode;
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function getUrl(): string
    {
        return sprintf('%s/%s', rtrim($this->baseUrl, '/'), ltrim($this->sufixUrl, '/'));
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): void
    {
        foreach ($parameters as $paramName => $paramValue) {
            $paramType = 'none';
            if (array_key_exists($paramName, $this->mandatoryParameters)) {
                $paramType = $this->mandatoryParameters[$paramName];
            }

            if (array_key_exists($paramName, $this->optionalParameters)) {
                $paramType = $this->optionalParameters[$paramName];
            }

            if (strtolower(gettype($paramValue)) == strtolower($paramType)) {
                $this->parameters[$paramName] = $paramValue;
            }
        }
    }

    public function checkParameters(): void
    {
        $mandatoryParameters = array_intersect_key($this->parameters, $this->mandatoryParameters);

        if (sizeof($this->mandatoryParameters) !== sizeof($mandatoryParameters)) {
            throw new DomainException('Not enough parameters in Request');
        }
    }

    public function getParametersJson(): string
    {
        return json_encode($this->getParameters());
    }

    public function mustVerifySSL(): bool
    {
        return (isset($this->verifySSL)) ? $this->verifySSL : false;
    }

    public function getHeaders(): array
    {
        $formatedHeaders = [];
        foreach ($this->headers as $key => $value) {
            $formatedHeaders[] = sprintf('%s:%s', $key, $value);
        }

        return $formatedHeaders;
        // return array_map(fn ($key, $value) => sprintf('%s:%s', $key, $value), $this->headers);
    }

    public function getHeadersJson(): string
    {
        return json_encode($this->getHeaders());
    }

    public function getMethod(): string
    {
        return (isset($this->method)) ? $this->method : 'get';
    }

    public function itNeedsAuth(): bool
    {
        return $this->needAuth;
    }
}
