<?php

namespace Wemust\Domain\Request;

interface Interfaces
{
    public function getUrl(): string;

    public function setParameters(array $parameters): void;

    public function checkParameters(): void;

    public function getParameters(): array;

    public function getParametersJson(): string;

    public function mustVerifySSL(): bool;

    public function getHeaders(): array;

    public function getHeadersJson(): string;

    public function getMethod(): string;

    public function itNeedsAuth(): bool;

    public function formatResult(array $result): array;
}
