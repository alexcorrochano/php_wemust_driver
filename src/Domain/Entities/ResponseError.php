<?php

declare(strict_types=1);

namespace Wemust\Domain\Entities;

final class ResponseError
{
    private int $code           = self::ERROR_NOERROR;
    private string $msg         = '';
    private array $extra        = [];
    private string $fileName    = '';
    private int $fileLineNumber = 0;
    private array $stackTrace   = [];

    const ERROR_NOERROR    = 0;
    const ERROR_REQUEST    = 1;
    const ERROR_CONNECTION = 2;
    const ERROR_SERVER     = 3;

    public function setCode(int $errorCode): void
    {
        $this->code = $errorCode;
    }

    public function setMsg(string $message): void
    {
        $this->msg = $message;
    }

    public function setExtra(array $extraData): void
    {
        $this->extra = $extraData;
    }

    public function addExtra($type, $value): void
    {
        $this->extra[$type] = $value;
    }

    public function setFilename(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    public function setFileLineNumber(int $fileLineNumber): void
    {
        $this->fileLineNumber = $fileLineNumber;
    }

    public function getArray(): array
    {
        $output = [
            'error_code' => $this->code,
            'error_msg'  => $this->msg,
        ];

        // if ($_ENV['debug']) {
        //     $output['extra'] = $this->extra;
        // }

        return $output;
    }

    public function getJson(): string
    {
        return json_encode($this->getArray());
    }
}
