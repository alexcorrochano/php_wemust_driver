<?php

declare(strict_types=1);

namespace Wemust\Domain\Entities;

use DateTime;
use DateInterval;
use DomainException;

final class AccessToken
{
    private string   $accessToken;
    private DateTime $createTime;
    private DateTime $expiresTime;

    const ERR_MSG_TOKEN_NOT_VALID = 'Not valid AccessToken';
    const ERR_MSG_TOKEN_NOT_READY = 'Token create time is older than now';
    const ERR_MSG_TOKEN_EXPIRED   = 'Token has expired';

    final public function __construct(string $accessToken, string $createTime, int $expiresTime)
    {
        date_default_timezone_set('Asia/Macau');
        $this->accessToken  = $accessToken;
        $this->createTime   = DateTime::createFromFormat('Y-m-d H:i:s', $createTime);
        $aux                = DateTime::createFromFormat('Y-m-d H:i:s', $createTime);
        $this->expiresTime  = $aux->add(new DateInterval(sprintf('PT%dS', $expiresTime)));
        unset($aux);
    }

    public function isValid(): bool
    {
        $now   = new DateTime();

        if ($this->createTime > $now) {
            return false;
        }

        if ($this->expiresTime < $now) {
            return false;
        }

        return true;
    }

    public function getAccessToken(): string
    {
        if (!$this->isValid()) {
            throw new DomainException(self::ERR_MSG_TOKEN_NOT_VALID);
        }

        return $this->accessToken;
    }
}
