<?php

declare(strict_types=1);

namespace Wemust\Domain\Entities;

final class Response
{
    private int $status;
    private array $data;
    private ?ResponseError $error;

    const STATUS_OK    = 1;
    const STATUS_ERROR = 2;

    public function __construct()
    {
        $this->status = self::STATUS_OK;
        $this->data   = [];
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function addData(string $type, $value): void
    {
        $this->data[$type] = $value;
    }

    public function setError(ResponseError $error): void
    {
        $this->error  = $error;
        $this->status = self::STATUS_ERROR;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getError(): ?ResponseError
    {
        return isset($this->error) ? $this->error : null;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function isSuccess(): bool
    {
        if (self::STATUS_OK === $this->status) {
            return true;
        }

        return false;
    }
}
