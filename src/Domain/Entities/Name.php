<?php

declare(strict_types=1);

namespace Wemust\Domain\Entities;

class Name
{
    private string $nameEn;
    private string $nameZh;
    private string $firstNameEn;
    private string $lastNameEn;
    private string $firstNameZh;
    private string $lastNameZh;

    public function __construct(string $nameEn, $nameZh)
    {
        $this->nameEn = $this->normalizeName($nameEn);
        $this->nameZh = $this->normalizeName($nameZh);

        if ($this->nameEn === $this->nameZh) {
            $this->nameZh = '';
        }

        list($this->firstNameEn,
            $this->lastNameEn,
            $this->firstNameZh,
            $this->lastNameZh) = self::extractNameParts($this->nameEn, $this->nameZh);
    }

    public static function normalizeName(string $name): string
    {
        // detect if is chinese and return unmodified
        if (preg_match("/\p{Han}+/u", $name)) {
            return $name;
        }

        return ucwords(strtolower($name), " \t\r\n\f\v(,");
    }

    public static function extractNameParts($nameEn, $nameZh): array
    {
        list($firstNameEn, $lastNameEn) = self::extractFromNameEn($nameEn);
        $firstNameZh                    = $lastNameZh                    = '';

        if (str_replace(' ', '', $nameEn) !== str_replace(' ', '', $nameZh)) {
            list($firstNameZh, $lastNameZh) = self::extractFromNameZh($nameZh);
        }

        return [$firstNameEn, $lastNameEn, $firstNameZh, $lastNameZh];
    }

    public static function extractFromNameZh(string $nameZh): array
    {
        $firstName = $lastName = '';
        $nameZh    = str_replace(' ', '', $nameZh);
        $nameParts = mb_str_split($nameZh);

        if (sizeof($nameParts) > 1) {
            $lastName = array_shift($nameParts);
        }
        if (sizeof($nameParts) > 2) {
            $lastName = implode('', [$lastName, array_shift($nameParts)]);
        }
        if (sizeof($nameParts) > 0) {
            $firstName = trim(implode('', array_values($nameParts)));
        }

        return [$firstName, $lastName];
    }

    public static function extractFromNameEn(string $nameEn): array
    {
        if (strpos($nameEn, ',') !== false) {
            return self::extractReverseName($nameEn);
        }

        return self::extractName($nameEn);
    }

    public static function extractName(string $name): array
    {
        $nameParts = explode(' ', $name);
        $firstName = $lastName  = '';

        if (sizeof($nameParts) > 0) {
            $firstName = array_shift($nameParts);
        }

        if (sizeof($nameParts) > 0) {
            $lastName = trim(implode(' ', $nameParts));
        }

        return [$firstName, $lastName];
    }

    public static function extractReverseName(string $name): array
    {
        $nameParts = explode(',', $name);
        $firstName = $lastName  = '';

        if (sizeof($nameParts) > 0) {
            $lastName = array_shift($nameParts);
        }

        if (sizeof($nameParts) > 0) {
            $firstName = trim(implode(' ', $nameParts));
        }

        return [$firstName, $lastName];
    }

    public function getNameEn(): string
    {
        return $this->nameEn;
    }

    public function getNameZh(): string
    {
        return $this->nameZh;
    }

    public function getFirstNameEn(): string
    {
        return $this->firstNameEn;
    }

    public function getFirstNameZh(): string
    {
        return $this->firstNameZh;
    }

    public function getLastNameEn(): string
    {
        return $this->lastNameEn;
    }

    public function getLastNameZh(): string
    {
        return $this->lastNameZh;
    }

    public function getSimpleFirstname(): string
    {
        if (empty($this->nameZh)) {
            return $this->getFirstNameEn();
        }

        return sprintf('%s%s', $this->getLastNameZh(), $this->getFirstNameZh());
    }

    public function getSimpleLastname(): string
    {
        if (empty($this->nameZh)) {
            return $this->getLastNameEn();
        }

        return sprintf('%s %s', $this->getLastNameEn(), $this->getFirstNameEn());
    }

    public function getFullName(): array
    {
        $output = [
            'simpleFirstname' => $this->getSimpleFirstname(),
            'simpleLastname'  => $this->getSimpleLastname(),
            'en'              => [
                'firstName' => $this->firstNameEn,
                'lastName'  => $this->lastNameEn,
            ],
            'zh' => [
                'firstName' => $this->firstNameZh,
                'lastName'  => $this->lastNameZh,
            ],
        ];

        return $output;
    }

    public function getFullNameInJson(): string
    {
        return json_encode($this->getFullName(), JSON_UNESCAPED_UNICODE);
    }
}
