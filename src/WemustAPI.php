<?php

declare(strict_types=1);

namespace Wemust;

use Wemust\Factories\RequestFactoryInterfaces as RequestFactory;
use Wemust\Managers\RequestManagerInterface as RequestManager;
use Wemust\Domain\Entities\Response;

final class WemustAPI
{
    private RequestFactory $factoryRequest;
    private RequestManager $requestManager;

    public function __construct(RequestFactory $requestFactory, RequestManager $requestManager)
    {
        $this->factoryRequest = $requestFactory;
        $authRequest          = $this->factoryRequest->getAuthRequest();
        $this->requestManager = $requestManager;
        $this->requestManager->setAuthRequest($authRequest);
    }

    public function __invoke(string $endPoint, array $parameters): Response
    {
        $request     = $this->factoryRequest->getRequest($endPoint, $parameters);

        return $this->requestManager->makeRequest($request);
    }
}
