<?php

declare(strict_types=1);

namespace Wemust\Managers;

use DomainException;
use Wemust\Domain\Entities\AccessToken;
use Wemust\Domain\Request\Interfaces as RequestInterface;
use Wemust\Service\Interfaces as ServiceInterfaces;

class Base
{
    protected ?AccessToken $accessToken;
    protected RequestInterface $authRequest;
    protected ServiceInterfaces $requestService;

    public function __construct(ServiceInterfaces $requestService)
    {
        $this->requestService = $requestService;
    }

    final public function setAuthRequest(RequestInterface $authRequest): void
    {
        $this->authRequest = $authRequest;
    }
}
