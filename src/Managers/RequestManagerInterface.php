<?php

declare(strict_types=1);

namespace Wemust\Managers;

use Wemust\Domain\Request\Interfaces as Request;
use Wemust\Domain\Entities\Response;

interface RequestManagerInterface
{
    public function makeRequest(Request $request): Response;

    public function setAuthRequest(Request $request): void;
}
