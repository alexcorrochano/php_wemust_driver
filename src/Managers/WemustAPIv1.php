<?php

declare(strict_types=1);

namespace Wemust\Managers;

use DomainException;
use Wemust\Domain\Request\Interfaces as Request;
use Wemust\Domain\Entities\AccessToken;
use Wemust\Domain\Entities\Response;
use Wemust\Domain\Entities\ResponseError;
use Wemust\Managers\Base as BaseManager;

final class WemustAPIv1 extends BaseManager implements RequestManagerInterface
{
    public function makeRequest(Request $request): Response
    {
        try {
            if ($request->itNeedsAuth()) {
                $accessToken = $this->getAccessKeyValue();
                $request->setParameters(['accessToken' => $accessToken]);
            }
            $request->checkParameters();
            $response = $this->requestService->__invoke($request);
        } catch (\Exception $exception) {
            $response = new Response();
            $error    = new ResponseError();
            $error->setMsg($exception->getMessage());
            $response->setError($error);
        }

        return $response;
    }

    private function getAccessKeyValue(): string
    {
        try {
            $accessKeyValue = '';

            if (!isset($this->accessToken)) {
                $this->requestAccessKey();
            }

            if (isset($this->accessToken)) {
                $accessKeyValue = $this->accessToken->getAccessToken();
            }
        } catch (\DomainException $exception) {
            if ($this->requestAccessKey()) {
                $accessKeyValue = $this->accessToken->getAccessToken();
            }
        }

        if (empty($accessKeyValue)) {
            throw new DomainException('Cannot get Access Token');
        }

        return $accessKeyValue;
    }

    private function requestAccessKey(): bool
    {
        if (!isset($this->authRequest)) {
            return false;
        }

        $response = $this->makeRequest($this->authRequest);
        if ($response->isSuccess()) {
            $responseData      = $response->getData();
            $this->accessToken = new AccessToken(
                $responseData['accessToken'],
                $responseData['createTime'],
                intval($responseData['expiresTime'])
            );

            return true;
        }

        return false;
    }
}
