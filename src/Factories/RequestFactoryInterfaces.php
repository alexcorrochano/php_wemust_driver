<?php

declare(strict_types=1);

namespace Wemust\Factories;

use Wemust\Domain\Request\Interfaces as RequestInterfaces;

interface RequestFactoryInterfaces
{
    public function getRequest(string $requestType, array $parameters): RequestInterfaces;

    public function getAuthRequest(): RequestInterfaces;
}
