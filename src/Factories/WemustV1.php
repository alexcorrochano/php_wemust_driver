<?php

declare(strict_types=1);

namespace Wemust\Factories;

use Wemust\Domain\Request\Interfaces as RequestInterface;
use Wemust\Domain\Request\V1\GetAccessToken;

final class WemustV1 implements RequestFactoryInterfaces
{
    private string $baseUrl;
    private string $serviceCode;
    private string $clientId;
    private string $clientSecret;

    private RequestInterface $authRequest;

    public function __construct(string $baseUrl, $serviceCode, $clientId, $clientSecret)
    {
        $this->baseUrl        = $baseUrl;
        $this->serviceCode    = $serviceCode;
        $this->clientId       = $clientId;
        $this->clientSecret   = $clientSecret;
    }

    public function getRequest(string $requestType, array $parameters): RequestInterface
    {
        $requestName = ucfirst($requestType);
        if ($requestName === 'GetAccessToken') {
            $parameters['clientId']     = $this->clientId;
            $parameters['clientSecret'] = $this->clientSecret;
        }

        $parameters['serviceCode']  = $this->serviceCode;

        $className = sprintf('Wemust\\Domain\\Request\\V1\\%s', ucfirst($requestType));
        if (class_exists($className)) {
            return new $className($parameters, $this->baseUrl, $this->serviceCode, $this->clientId, $this->clientSecret);
        }
    }

    public function getAuthRequest(): RequestInterface
    {
        if (!isset($this->authRequest)) {
            $authParameters = [
                'serviceCode'  => $this->serviceCode,
                'clientId'     => $this->clientId,
                'clientSecret' => $this->clientSecret,
            ];

            $this->authRequest = new GetAccessToken($authParameters, $this->baseUrl, $this->serviceCode, $this->clientId, $this->clientSecret);
        }

        return $this->authRequest;
    }
}
