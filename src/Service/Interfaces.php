<?php

namespace Wemust\Service;

use Wemust\Domain\Request\Interfaces as RequestInterfaces;
use Wemust\Domain\Entities\Response;

interface Interfaces
{
    public function __invoke(RequestInterfaces $request): Response;
}
