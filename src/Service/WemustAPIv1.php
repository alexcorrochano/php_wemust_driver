<?php

declare(strict_types=1);

namespace Wemust\Service;

use Wemust\Domain\Entities\ResponseError;
use Wemust\Domain\Request\Interfaces as RequestInterfaces;
use Wemust\Domain\Entities\Response;

// use Wemust\Service\Interfaces as RequestServiceInterface;

class WemustAPIv1 implements Interfaces
{
    public function __invoke(RequestInterfaces $request): Response
    {
        $requestUrl         = $request->getUrl();
        $requestParameters  = $request->getParameters();
        $requestHeaders     = $request->getHeaders();
        $requestMethod      = $request->getMethod();

        $response       = new Response();
        $error          = new ResponseError();

        $errorExtraInfo = [
            'url'     => $requestUrl,
            'param'   => $requestParameters,
            'headers' => $requestHeaders,
            'method'  => $requestMethod,
        ];

        if (strtolower($requestMethod) === 'get') {
            $httpRequest = $this->composeHttpGetRequest($requestUrl, $requestParameters, $requestHeaders);
        } elseif (strtolower($requestMethod) === 'post') {
            $httpRequest = $this->composeHttpPostRequest($requestUrl, $requestParameters, $requestHeaders);
        } else {
            $error->setCode(ResponseError::ERROR_CONNECTION);
            $error->setMsg('Not supported request method');
            $error->setExtra($errorExtraInfo);
            $response->setError($error);

            return $response;
        }

        try {
            $serverResponse       = curl_exec($httpRequest);
            $serverResponseStatus = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
        } catch (\Exception $exception) {
            $error->setCode(ResponseError::ERROR_CONNECTION);
            $error->setMsg($exception->getMessage());
            $error->setExtra($errorExtraInfo);
            $response->setError($error);

            return $response;
        } finally {
            if (false !== $httpRequest) {
                curl_close($httpRequest);
            }
        }

        if (false === $serverResponse) {
            $error->setCode(ResponseError::ERROR_REQUEST);
            $error->setMsg('No answer from server');
            $error->setExtra($errorExtraInfo);

            $response->setError($error);

            return $response;
        }

        if ($serverResponseStatus !== 200) {
            $error->setCode(ResponseError::ERROR_REQUEST);
            $error->setMsg('Bad answer');
            $error->setExtra($errorExtraInfo);
            $error->addExtra('raw_server_answer', var_export($serverResponse, true));
            $error->addExtra('response_status', $serverResponseStatus);
            $response->setError($error);

            return $response;
        }

        $result         = json_decode($serverResponse, true);
        $formatedResult = $request->formatResult($result);
        $response->setData($formatedResult);

        return $response;
    }

    private function composeHttpGetRequest(string $url, array $parameters = [], array $headers = [])
    {
        $urlWithParameters = sprintf('%s?%s', $url, http_build_query($parameters, '', '&'));
        $httpRequest       = curl_init($urlWithParameters);

        if (false === $httpRequest) {
            return false;
        }

        return $this->completeHttpRequest($httpRequest, $headers);
    }

    private function composeHttpPostRequest(string $url, array $parameters = [], array $headers = [])
    {
        $httpRequest = curl_init($url);

        if (false === $httpRequest) {
            return false;
        }

        $parametersAsJson = json_encode($parameters);

        curl_setopt($httpRequest, CURLOPT_POST, 1);//设置为POST方式
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $parametersAsJson);

        return $this->completeHttpRequest($httpRequest, $headers);
    }

    private function completeHttpRequest($httpRequest, array $headers)
    {
        if (false) {
            curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        }

        if (sizeof($headers) > 0) {
            curl_setopt($httpRequest, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Moodle server');
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HEADER, 0);

        return $httpRequest;
    }
}
