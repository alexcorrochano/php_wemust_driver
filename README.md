# Wemust API driver for PHP

## Installation

To install this driver, run the command below and you will get the latest version.

```bash
composer require alexander.corrochano/php_wemust_driver
```

## How to use it

Bellow you can find an example of use. Don't forget to use your own credentials.

```php
    $requestFactory = new RequestFactory(
        $baseUrl,       // Base url where Wemust API is deployed
        $serviceCode,   // The name of your app that must be register in Wemust API
        $clientID,      // The ID of your app (provided by wemust operator)
        $clientSecret   // The secret of your app (provided by wemust operator)
    );
    $requestService = new RequestService();
    $requestManager = new RequestManager($requestService);

    $wemust   = new WemustAPI($requestFactory, $requestManager);
    $response = $wemust('GetUserInfoByName', ['username' => 'ancalexander']);
    $status   = $response->getStatus(); // return 1 if there is no errors
    $data     = $response->getData();   // return an array with all data reported by Wemust API
```
